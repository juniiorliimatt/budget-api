package br.com.budget.repositories;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.entities.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

@DataJpaTest
@ActiveProfiles("test")
class AdditionalIncomeRepositoryTests {

    private final AdditionalIncomeRepository additionalIncomeRepository;

    private AdditionalIncome additionalIncome;

    private UUID existingId;

    private UUID nonExistingId;

    @Autowired
    public AdditionalIncomeRepositoryTests(AdditionalIncomeRepository additionalIncomeRepository) {
        this.additionalIncomeRepository = additionalIncomeRepository;
    }

    @BeforeEach
    void setup() {
        additionalIncome = additionalIncomeRepository.save(Factory.createAdditionalIncome());
        this.existingId = additionalIncome.getId();
        this.nonExistingId = UUID.randomUUID();
    }

    @Test
    void saveShouldANewObject() {
        var newAdditionalIncome = AdditionalIncome.builder()
                                                                .name("FÉRIAS")
                                                                .build();

        newAdditionalIncome = additionalIncomeRepository.save(newAdditionalIncome);

        Assertions.assertNotNull(newAdditionalIncome.getId());
        Assertions.assertEquals("FÉRIAS", newAdditionalIncome.getName());
    }


    @Test
    void findByIdShouldFindAnObjectWhenIdIsPresent() {
        final var optional = additionalIncomeRepository.findById(existingId);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    void findByIdNotShouldFindAnObjectWhenIdIsNotPresent() {
        final var optional = additionalIncomeRepository.findById(nonExistingId);
        Assertions.assertTrue(optional.isEmpty());
    }

    @Test
    void deleteShouldDeleteObjectWhenIdExists() {
        additionalIncomeRepository.deleteById(existingId);
        final var optional = additionalIncomeRepository.findById(existingId);
        Assertions.assertFalse(optional.isPresent());
    }

    @Test
    void shouldThrowEmptyResultDataAccessExceptionWhenIdNotExists(){
        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> additionalIncomeRepository.deleteById(nonExistingId));
    }
}
