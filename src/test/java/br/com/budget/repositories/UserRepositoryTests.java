package br.com.budget.repositories;

import br.com.budget.entities.Role;
import br.com.budget.entities.User;
import br.com.budget.entities.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;

import java.util.UUID;

@DataJpaTest
@ActiveProfiles("test")
class UserRepositoryTests {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private User user;

    private Role roleAdmin;

    private Role roleUser;

    private UUID existingId;

    private UUID nonExistingId;

    @Autowired
    public UserRepositoryTests(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @BeforeEach
    void setup() {
        roleAdmin = roleRepository.save(Role.builder().id(1L).authority("ADMIN").build());
        roleUser = roleRepository.save(Role.builder().id(2L).authority("USER").build());

        user = userRepository.save(Factory.createUser());

        this.existingId = user.getId();
        this.nonExistingId = UUID.randomUUID();
    }

    @Test
    void saveShouldANewObject() {
        user.getRoles().add(roleAdmin);
        user.getRoles().add(roleUser);

        var newUser = userRepository.save(user);

        Assertions.assertNotNull(newUser.getId());
        Assertions.assertEquals("User", user.getFirstName());
    }

    @Test
    void findByIdShouldFindAnObjectWhenIdIsPresent() {
        var optional = userRepository.findById(existingId);
        Assertions.assertTrue(optional.isPresent());
    }

    @Test
    void findByIdNotShouldFindAnObjectWhenIdIsNotPresent() {
        var user = userRepository.findById(nonExistingId);
        Assertions.assertTrue(user.isEmpty());
    }

    @Test
    void deleteShouldDeleteObjectWhenIdExists() {
        userRepository.deleteById(existingId);
        var result = userRepository.findById(existingId);
        Assertions.assertFalse(result.isPresent());
    }

    @Test
    void shouldThrowEmptyResultDataAccessExceptionWhenIdNotExists() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                                () -> userRepository.deleteById(nonExistingId));
    }


}
