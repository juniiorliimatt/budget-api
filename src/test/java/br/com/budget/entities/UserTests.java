package br.com.budget.entities;

import br.com.budget.entities.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.sql.Timestamp;
import java.util.UUID;

@ActiveProfiles("test")
class UserTests {

    private User user;

    @BeforeEach
    void setup() {
        var roleUser = Factory.createRole(2L, "USER");
        user = Factory.createUser();
        user.getRoles().add(roleUser);
    }

    @Test
    void shouldCreateANewUser() {
        Assertions.assertNotNull(user);
        Assertions.assertEquals("User", user.getFirstName());
    }

    @Test
    void shouldCheckUser() {
        Assertions.assertEquals(UUID.class, user.getId().getClass());
        Assertions.assertEquals("User", user.getFirstName());
        Assertions.assertEquals("Test", user.getLastName());
        Assertions.assertEquals("user@gmail.com", user.getEmail());
        Assertions.assertEquals("user@gmail.com", user.getUsername());
        Assertions.assertEquals("12345", user.getPassword());

        Assertions.assertFalse(user.getRoles().isEmpty());
        Assertions.assertFalse(user.getAuthorities().isEmpty());

        Assertions.assertEquals(Timestamp.class, user.getCreatedAt().getClass());
        Assertions.assertEquals(Timestamp.class, user.getUpdatedAt().getClass());

        Assertions.assertTrue(user.getIsAccountNonExpired());
        Assertions.assertTrue(user.getIsAccountNonLocked());
        Assertions.assertTrue(user.getIsCredentialsNonExpired());
        Assertions.assertTrue(user.getIsEnabled());
        Assertions.assertTrue(user.isAccountNonExpired());
        Assertions.assertTrue(user.isAccountNonLocked());
        Assertions.assertTrue(user.isEnabled());
        Assertions.assertTrue(user.isCredentialsNonExpired());

        user.prePersist();
    }

    @Test
    void shouldUpdateUser() {
        var id = UUID.randomUUID();
        user.setId(id);
        user.setFirstName("New user first name");
        user.setLastName("New user last name");
        user.setEmail("newuser@gmail.com");
        user.setPassword("newUserPassword");

        user.setIsAccountNonExpired(Boolean.FALSE);
        user.setIsAccountNonLocked(Boolean.FALSE);
        user.setIsCredentialsNonExpired(Boolean.FALSE);
        user.setIsEnabled(Boolean.FALSE);

        Assertions.assertEquals(id, user.getId());
        Assertions.assertEquals("New user first name", user.getFirstName());
        Assertions.assertEquals("New user last name", user.getLastName());
        Assertions.assertEquals("newuser@gmail.com", user.getEmail());
        Assertions.assertEquals("newUserPassword", user.getPassword());

        Assertions.assertFalse(user.isAccountNonExpired());
        Assertions.assertFalse(user.isAccountNonLocked());
        Assertions.assertFalse(user.isEnabled());
        Assertions.assertFalse(user.isCredentialsNonExpired());
    }

    @Test
    void shouldCheckIfTwoUsersAreEqual() {
        var otherUser = Factory.createUser();
        Assertions.assertNotEquals(otherUser, user);
    }

    @Test
    void shouldCheckIfObjectIsUserType() {
        var userOne = new User();
        var userTwo = new User();

        Assertions.assertEquals(User.class, userOne.getClass());
        Assertions.assertEquals(User.class, userTwo.getClass());
    }
}
