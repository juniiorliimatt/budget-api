package br.com.budget.entities;

import br.com.budget.entities.utils.Factory;
import br.com.budget.enums.SpentType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@ActiveProfiles("test")
class SpendingTests {

    private Spending spending;

    private String name;

    private String description;

    @BeforeEach
    void setup() {
        spending = Factory.createSpending();
        name = "Nubank";
        description = "Dispesas com cartão de crédito";
    }

    @Test
    void shouldCreateANewSpending() {
        var newSpending = new Spending();
        Assertions.assertNotNull(newSpending);
        Assertions.assertEquals(Spending.class, newSpending.getClass());
    }

    @Test
    void shouldCheckSpending() {
        Assertions.assertEquals(UUID.class, spending.getId().getClass());
        Assertions.assertEquals(name, spending.getName());
        Assertions.assertEquals(description, spending.getDescription());
        Assertions.assertEquals(new BigDecimal("200.00"), spending.getValue());
        Assertions.assertEquals(LocalDate.now(), spending.getDate());
        Assertions.assertEquals(Boolean.TRUE, spending.getWasPaid());
        Assertions.assertEquals(SpentType.ESSENTIAL, spending.getSpentType());
        Assertions.assertEquals(Timestamp.class, spending.getCreatedAt().getClass());
        Assertions.assertEquals(Timestamp.class, spending.getUpdatedAt().getClass());
    }

    @Test
    void shouldUpdateSpending() {
        var newSpending = new Spending();
        newSpending.setId(UUID.randomUUID());
        newSpending.setDate(LocalDate.now());
        newSpending.setName("Nome Atualizado");
        newSpending.setValue(BigDecimal.valueOf(3000.00));

        Assertions.assertNotEquals(newSpending, spending);
    }
}
