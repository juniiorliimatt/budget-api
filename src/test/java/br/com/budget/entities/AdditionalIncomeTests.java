package br.com.budget.entities;

import br.com.budget.entities.utils.Factory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

@ActiveProfiles("test")
class AdditionalIncomeTests {

    @Test
    void shouldCreateANewAdditionalIncomeObject() {
        var newAdditionalIncome = new AdditionalIncome();

        Assertions.assertNotNull(newAdditionalIncome);
        Assertions.assertEquals(AdditionalIncome.class, newAdditionalIncome.getClass());
    }

    @Test
    void shouldCreateANewAdditionalIncome() {
        var additionalIncome = Factory.createAdditionalIncome();
        Assertions.assertNotNull(additionalIncome);

        Assertions.assertEquals(UUID.class, additionalIncome.getId().getClass());
        Assertions.assertEquals("FGTS-"+ LocalDate.now(), additionalIncome.getName());
        Assertions.assertEquals(new BigDecimal("500.00"), additionalIncome.getValue());

        Assertions.assertEquals(Timestamp.class, additionalIncome.getCreatedAt().getClass());
        Assertions.assertEquals(Timestamp.class, additionalIncome.getUpdatedAt().getClass());
    }

    @Test
    void shouldUpdateAAdditionalIncome() {
        var additionalIncome = Factory.createAdditionalIncome();
        var newAdditionalIncome = new AdditionalIncome();
        newAdditionalIncome.setId(UUID.randomUUID());
        newAdditionalIncome.setName("13");
        newAdditionalIncome.setValue(new BigDecimal("1500.00"));
        newAdditionalIncome.setRevenue(new Revenue());

        Assertions.assertNotEquals(additionalIncome, newAdditionalIncome);

        Assertions.assertNotEquals(additionalIncome.getId(), newAdditionalIncome.getId());
        Assertions.assertNotEquals(additionalIncome.getName(), newAdditionalIncome.getName());
        Assertions.assertNotEquals(additionalIncome.getValue(), newAdditionalIncome.getValue());
        Assertions.assertNotEquals(additionalIncome.getRevenue(), newAdditionalIncome.getRevenue());
    }
}
