--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table additional_income
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE additional_income
(
    id         UUID NOT NULL PRIMARY KEY,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    name       VARCHAR(20),
    value      NUMERIC(7,2),
    revenue_id UUID,

    CONSTRAINT FK_ADDITIONAL_INCOME_REVENUE FOREIGN KEY(revenue_id) REFERENCES revenues(id)
)
