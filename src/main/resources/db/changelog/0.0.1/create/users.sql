--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table users
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE users
(
    id         UUID NOT NULL primary key,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    first_name VARCHAR(100),
    last_name  VARCHAR(100),
    email      VARCHAR(100) UNIQUE,
    password   VARCHAR(100),
    is_account_non_expired BOOLEAN,
    is_account_non_locked BOOLEAN,
    is_credentials_non_expired BOOLEAN,
    is_enabled BOOLEAN

)
