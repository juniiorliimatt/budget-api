--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table user_roles
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE user_roles
(
    user_id UUID,
    role_id BIGINT,

    CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES users(id),
    CONSTRAINT fk_role_id FOREIGN KEY(role_id) REFERENCES roles(id)
)
