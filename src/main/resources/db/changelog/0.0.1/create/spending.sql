--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table spending
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE spending
(
    id          UUID NOT NULL primary key,
    name        VARCHAR(25),
    description VARCHAR(250),
    value       NUMERIC(7, 2),
    date        DATE,
    was_paid    BOOLEAN,
    spent_type  VARCHAR(10),
    created_at  TIMESTAMP,
    updated_at  TIMESTAMP
)
