--liquibase formatted sql

-- changeset author:junior.lima
-- comment insert table roles
-- preconditions onFail:MARK_RAN onError:HALT
INSERT INTO roles (id, authority, created_at, updated_at) VALUES ('1', 'ADMIN', now(), now());
INSERT INTO roles (id, authority, created_at, updated_at) VALUES ('2', 'USER', now(), now());
