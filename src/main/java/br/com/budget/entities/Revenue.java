package br.com.budget.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 22/05/2022
 */

@Entity
@Table(name = "revenues")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Revenue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @NotBlank
    private String name;

    @Column(nullable = false, unique = true)
    private BigDecimal wage;
    @Column(nullable = false)
    private LocalDate date;

    @CreationTimestamp
    private Timestamp createdAt;

    @UpdateTimestamp
    private Timestamp updatedAt;

    @Builder.Default
    @OneToMany(mappedBy = "revenue", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<AdditionalIncome> additionalIncoming = new ArrayList<>();

    @PrePersist
    public void prePersist() {
        name = date.getMonth() + "-" + LocalDate.now().getYear();
        additionalIncoming.forEach(additionalIncome -> additionalIncome.setRevenue(this));
    }

}
