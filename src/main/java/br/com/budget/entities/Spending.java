package br.com.budget.entities;

import br.com.budget.enums.SpentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.UUID;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 30/05/2022
 */

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class Spending implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Size(min = 3, max = 25)
    @NotBlank(message = "Required field name")
    @Column(nullable = false)
    private String name;

    @Size(min = 5, max = 250)
    private String description;

    @NotNull(message = "Required field value")
    private BigDecimal value;

    @NotNull(message = "Required field date")
    private LocalDate date;

    @NotNull(message = "Required field wasPaid")
    private Boolean wasPaid;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Required field spentType")
    private SpentType spentType;

    @CreationTimestamp
    private Timestamp createdAt;

    @UpdateTimestamp
    private Timestamp updatedAt;

}
