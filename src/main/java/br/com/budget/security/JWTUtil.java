package br.com.budget.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@Component
public class JWTUtil {

  @Value("${jwt_secret}")
  private String secret;

  public String generateToken(String email) throws IllegalArgumentException, JWTCreationException {
	return JWT.create()
			  .withSubject("User Details")
			  .withClaim("email", email)
			  .withIssuedAt(new Date())
			  .withIssuer("Budget api")
			  .sign(Algorithm.HMAC256(secret));
  }

  public String validateTokenAndRetrieveSubject(String token) throws JWTVerificationException {
	JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret))
							  .withSubject("User Details")
							  .withIssuer("Budget api")
							  .build();
	DecodedJWT jwt = verifier.verify(token);
	return jwt.getClaim("email")
			  .asString();
  }

}
