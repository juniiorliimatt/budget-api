package br.com.budget.security;

import br.com.budget.services.UserService;
import com.auth0.jwt.exceptions.JWTVerificationException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@Component
@RequiredArgsConstructor
public class JWTFilter extends OncePerRequestFilter {

  private final UserService userService;
  private final JWTUtil jwtUtil;

  @Override
  protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
								  final FilterChain filterChain) throws ServletException, IOException {
	final var authHeader = request.getHeader("Authorization");
	if(authHeader != null && !authHeader.isBlank() && authHeader.startsWith("Bearer ")) {
	  String jwt = authHeader.substring(7);
	  if(jwt == null || jwt.isBlank()) {
		response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid JWT Token in Bearer Header");
	  } else {
		try {
		  final var email = jwtUtil.validateTokenAndRetrieveSubject(jwt);
		  final var userDetails = userService.loadUserByUsername(email);
		  var authToken = new UsernamePasswordAuthenticationToken(email, userDetails.getPassword(),
																  userDetails.getAuthorities());
		  if(SecurityContextHolder.getContext()
								  .getAuthentication() == null) {

			SecurityContextHolder.getContext()
								 .setAuthentication(authToken);
		  }
		} catch(JWTVerificationException exc) {
		  response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid JWT Token");
		}
	  }
	}

	filterChain.doFilter(request, response);
  }

}
