package br.com.budget.controllers;

import br.com.budget.models.Response;
import br.com.budget.models.dto.UserDTO;
import br.com.budget.models.dto.UserInsertDTO;
import br.com.budget.models.dto.UserUpdateDTO;
import br.com.budget.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;

  @GetMapping
  public ResponseEntity<Page<UserDTO>> findAll(Pageable pageable) {
	var users = userService.findAll(pageable);
	return ResponseEntity.ok()
						 .body(users);
  }

  @GetMapping("/info")
  public ResponseEntity<Response> getUserInfo() {
	var user = userService.getUserDetails();
	return ResponseEntity.ok()
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.OK.value())
									   .status(HttpStatus.OK)
									   .data(Map.of("User info: ", user))
									   .build());
  }

  @GetMapping("/{id}")
  public ResponseEntity<Response> findById(@PathVariable("id") UUID id) {
	var user = userService.findById(id);
	return ResponseEntity.ok()
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.OK.value())
									   .status(HttpStatus.OK)
									   .data(Map.of("User info: ", user))
									   .build());
  }

  @PostMapping
  public ResponseEntity<Response> save(@RequestBody UserInsertDTO userInsertDTO) {
	var dto = userService.save(userInsertDTO);
	var uri = ServletUriComponentsBuilder.fromCurrentRequest()
										 .path("/{id}")
										 .buildAndExpand(dto.getId())
										 .toUri();
	return ResponseEntity.created(uri)
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.CREATED.value())
									   .status(HttpStatus.CREATED)
									   .message("URI - " + uri)
									   .data(Map.of("new user: ", dto))
									   .build());

  }

  @PutMapping
  public ResponseEntity<Response> update(@RequestBody UserUpdateDTO userUpdateDTO) {
	var user = userService.update(userUpdateDTO);
	return ResponseEntity.ok()
						 .body(Response.builder()
									   .timeStamp(LocalDateTime.now())
									   .statusCode(HttpStatus.OK.value())
									   .status(HttpStatus.OK)
									   .data(Map.of("User updated: ", user))
									   .build());
  }

}
