package br.com.budget.controllers;

import br.com.budget.models.FiftyThirtyTwenty;
import br.com.budget.models.Response;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.models.dto.RevenueDTO;
import br.com.budget.models.dto.TotalDTO;
import br.com.budget.services.RevenueService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@RestController
@RequestMapping("/revenue")
@RequiredArgsConstructor
public class RevenueController {

    private final RevenueService revenueService;

    @GetMapping
    public ResponseEntity<Page<RevenueDTO>> findAll(Pageable pageable) {
        var pageRevenueDTO = revenueService.findAll(pageable);
        return ResponseEntity.ok().body(pageRevenueDTO);
    }

    @GetMapping("/{date}")
    public ResponseEntity<Page<RevenueDTO>> findByDate(Pageable pageable, @PathVariable("date") LocalDate date) {
        var pageRevenueDTO = revenueService.findByDate(pageable, date);
        return ResponseEntity.ok().body(pageRevenueDTO);
    }

    @GetMapping("/rule")
    public ResponseEntity<FiftyThirtyTwenty> findFiftyThirtyTwentyByDate(@RequestParam("date") String date) {
        final var initialDate = LocalDate.from(DateTimeFormatter.ISO_LOCAL_DATE.parse(date));
        var fiftythirtytwenty = revenueService.fiftythirtytwenty(initialDate);
        return ResponseEntity.ok().body(fiftythirtytwenty);
    }

    @GetMapping("/total/{name}")
    public TotalDTO getTotal(@PathVariable("name") String name) {
        return revenueService.getTotal(name);
    }

    @PostMapping
    public ResponseEntity<Response> save(@RequestBody RevenueDTO revenueDTO) {
        revenueDTO = revenueService.save(revenueDTO);
        var uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(revenueDTO.getId()).toUri();
        return ResponseEntity.created(uri).body(Response.builder()
                                                        .timeStamp(LocalDateTime.now())
                                                        .statusCode(HttpStatus.CREATED.value())
                                                        .status(HttpStatus.CREATED)
                                                        .data(Map.of("New Revenue: ", revenueDTO))
                                                        .build());
    }

    @PutMapping
    public ResponseEntity<Response> update(@RequestBody RevenueDTO revenueDTO) {
        revenueDTO = revenueService.update(revenueDTO);
        return ResponseEntity.ok().body(Response.builder()
                                                .timeStamp(LocalDateTime.now())
                                                .statusCode(HttpStatus.OK.value())
                                                .status(HttpStatus.OK)
                                                .data(Map.of("Revenue updated: ", revenueDTO))
                                                .build());
    }

    @DeleteMapping
    public ResponseEntity<Response> delete(@RequestBody EntityIdDTO entityIdDTO) {
        revenueService.delete(entityIdDTO);
        return ResponseEntity.ok().body(Response.builder()
                                                .timeStamp(LocalDateTime.now())
                                                .statusCode(HttpStatus.OK.value())
                                                .status(HttpStatus.OK)
                                                .data(Map.of("Revenue deleted: ", entityIdDTO.getId()))
                                                .build());
    }
}
