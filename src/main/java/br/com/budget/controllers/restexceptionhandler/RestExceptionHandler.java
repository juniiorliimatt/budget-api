package br.com.budget.controllers.restexceptionhandler;

import br.com.budget.models.ErrorResponse;
import br.com.budget.models.FieldError;
import br.com.budget.services.exceptions.DatabaseException;
import br.com.budget.services.exceptions.LoginInvalidException;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundException(final ResourceNotFoundException exception,
                                                                         final HttpServletRequest request) {
        final var status = HttpStatus.NOT_FOUND;
        final var errorResponse = ErrorResponse.builder()
                                               .timestamp(Instant.now())
                                               .status(HttpStatus.NOT_FOUND.value())
                                               .statusName(status.name())
                                               .message(exception.getMessage())
                                               .path(request.getRequestURI())
                                               .exception(exception.getClass()
                                                                   .getSimpleName())
                                               .build();

        return ResponseEntity.status(status)
                             .body(errorResponse);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleMethodArgumentNotValid(final MethodArgumentNotValidException exception,
                                                                      final HttpServletRequest request) {
        final var status = HttpStatus.BAD_REQUEST;
        final BindingResult bindingResult = exception.getBindingResult();
        final List<FieldError> fieldErrors = bindingResult.getFieldErrors()
                                                          .stream()
                                                          .map(error -> {
                                                              final FieldError fieldError = new FieldError();
                                                              fieldError.setErrorCode(error.getCode());
                                                              fieldError.setField(error.getField());
                                                              return fieldError;
                                                          })
                                                          .collect(Collectors.toList());

        final var errorResponse = ErrorResponse.builder()
                                               .timestamp(Instant.now())
                                               .status(status.value())
                                               .statusName(status.name())
                                               .message(exception.getMessage())
                                               .path(request.getRequestURI())
                                               .exception(exception.getClass()
                                                                   .getSimpleName())
                                               .fieldErrors(fieldErrors)
                                               .build();

        return ResponseEntity.status(status)
                             .body(errorResponse);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleConstraintViolationException(final ConstraintViolationException exception, final HttpServletRequest request) {
        final var status = HttpStatus.UNPROCESSABLE_ENTITY;
        final var errorResponse = ErrorResponse.builder()
                                               .timestamp(Instant.now())
                                               .status(status.value())
                                               .statusName(status.name())
                                               .message(exception.getMessage())
                                               .path(request.getRequestURI())
                                               .exception(exception.getClass()
                                                                   .getSimpleName())
                                               .fieldErrors(List.of())
                                               .build();

        return ResponseEntity.status(status)
                             .body(errorResponse);
    }

    @ExceptionHandler(DatabaseException.class)
    public ResponseEntity<ErrorResponse> handleDatabaseException(final DatabaseException exception,
                                                                 final HttpServletRequest request) {
        return getErrorResponseEntity(request, exception.getMessage(), exception.getClass()
                                                                                .getSimpleName());
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDataIntegrityViolationException(
            final DataIntegrityViolationException exception, final HttpServletRequest request) {
        return getErrorResponseEntity(request, exception.getMessage(), exception.getClass()
                                                                                .getSimpleName());
    }

    @ExceptionHandler(LoginInvalidException.class)
    public ResponseEntity<ErrorResponse> handleLoginInvalidExceptionException(final LoginInvalidException exception,
                                                                              final HttpServletRequest request) {
        return getErrorResponseEntity(request, exception.getMessage(), exception.getClass()
                                                                                .getSimpleName());
    }

    private ResponseEntity<ErrorResponse> getErrorResponseEntity(HttpServletRequest request, String message,
                                                                 String simpleName) {
        final var status = HttpStatus.BAD_REQUEST;
        final var errorResponse = ErrorResponse.builder()
                                               .timestamp(Instant.now())
                                               .status(status.value())
                                               .statusName(status.name())
                                               .message(message)
                                               .path(request.getRequestURI())
                                               .exception(simpleName)
                                               .build();

        return ResponseEntity.status(status)
                             .body(errorResponse);
    }
}
