package br.com.budget.controllers;

import br.com.budget.models.LoginCredentials;
import br.com.budget.models.dto.UserDTO;
import br.com.budget.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/register")
    public Map<String, String> register(@Valid @RequestBody final UserDTO userDTO) {
        return authService.register(userDTO);
    }

    @PostMapping("/login")
    public Map<String, String> login(@RequestBody final LoginCredentials loginCredentials) {
        return authService.login(loginCredentials);
    }

}
