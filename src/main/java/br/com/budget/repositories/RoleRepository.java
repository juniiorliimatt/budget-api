package br.com.budget.repositories;

import br.com.budget.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 14/05/2022
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> { }
