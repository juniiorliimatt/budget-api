package br.com.budget.repositories;

import br.com.budget.entities.Spending;
import br.com.budget.enums.SpentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 30/05/2022
 */

@Repository
public interface SpendingRepository extends JpaRepository<Spending, UUID> {
    Page<Spending> findByDate(Pageable pageable, LocalDate date);

    List<Spending> findByDate(LocalDate date);

    List<Spending> findByDateBetween(LocalDate dateInitial, LocalDate dateFinal);

    List<Spending> findByDateBetweenAndWasPaidIsTrue(LocalDate dateInitial, LocalDate dateFinal);

    List<Spending> findByDateBetweenAndWasPaidIsFalse(LocalDate dateInitial, LocalDate dateFinal);

    List<Spending> findByDateBetweenAndSpentType(LocalDate dateInitial, LocalDate dateFinal, SpentType spentType);

    List<Spending> findByDateBetweenAndWasPaidIsTrueAndSpentType(LocalDate dateInitial, LocalDate dateFinal, SpentType spentType);

    List<Spending> findByDateBetweenAndWasPaidIsFalseAndSpentType(LocalDate dateInitial, LocalDate dateFinal, SpentType spentType);

}
