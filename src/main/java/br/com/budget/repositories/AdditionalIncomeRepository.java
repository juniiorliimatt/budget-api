package br.com.budget.repositories;

import br.com.budget.entities.AdditionalIncome;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface AdditionalIncomeRepository extends JpaRepository<AdditionalIncome, UUID> {
    List<AdditionalIncome> findByNameContainingIgnoreCase(String name);
}
