package br.com.budget.repositories;

import br.com.budget.entities.Revenue;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RevenueRepository extends JpaRepository<Revenue, UUID> {

    Page<Revenue> findByDate(final Pageable pageable, final LocalDate date);

    List<Revenue> findByDateBetween(final LocalDate dateInitial, final LocalDate dateFinal);

    Optional<Revenue> findByDate(final LocalDate date);

    Optional<Revenue> findByName(final String name);
}
