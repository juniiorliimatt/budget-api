package br.com.budget.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FiftyThirtyTwenty implements Serializable {

    private BigDecimal essentials50;
    private BigDecimal personal30;
    private BigDecimal savings20;

}
