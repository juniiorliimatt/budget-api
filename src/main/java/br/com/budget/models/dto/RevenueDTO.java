package br.com.budget.models.dto;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.entities.Revenue;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RevenueDTO {

    private UUID id;

    private String name;

    private BigDecimal wage;

    private LocalDate date;

    private List<AdditionalIncome> additionalIncoming = new ArrayList<>();

    public RevenueDTO(Revenue revenue) {
        this.id = revenue.getId();
        this.name = revenue.getName();
        this.wage = revenue.getWage();
        this.date = revenue.getDate();
        this.additionalIncoming.addAll(revenue.getAdditionalIncoming());
    }
}
