package br.com.budget.models.dto;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.entities.Revenue;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class AdditionalIncomeDTO {

    private UUID id;
    private String name;
    private BigDecimal value;
    private Revenue revenue;

    public AdditionalIncomeDTO(AdditionalIncome additionalIncome) {
        this.id = additionalIncome.getId();
        this.name = additionalIncome.getName();
        this.value = additionalIncome.getValue();
        this.revenue = additionalIncome.getRevenue();
    }
}
