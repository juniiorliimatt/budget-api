package br.com.budget.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class TotalExpensePerMonthDTO {

    private BigDecimal totalSpend;
    private BigDecimal paidOut;
    private BigDecimal missing;
    private BigDecimal finalBalance;
    private BigDecimal currentBalance;

}
