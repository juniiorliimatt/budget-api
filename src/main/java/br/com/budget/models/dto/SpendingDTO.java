package br.com.budget.models.dto;

import br.com.budget.entities.Spending;
import br.com.budget.enums.SpentType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class SpendingDTO {

    private UUID id;

    private String name;

    private String description;

    private BigDecimal value;

    private LocalDate date;

    private Boolean wasPaid;

    private SpentType spentType;

    public SpendingDTO(Spending spending) {
        this.id = spending.getId();
        this.name = spending.getName();
        this.description = spending.getDescription();
        this.value = spending.getValue();
        this.date = spending.getDate();
        this.wasPaid = spending.getWasPaid();
        this.spentType = spending.getSpentType();
    }
}
