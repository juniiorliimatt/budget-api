package br.com.budget.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class GrandTotalDTO {

    private BigDecimal spendTotal;
    private BigDecimal paidTotal;
    private BigDecimal missingTotal;
    private BigDecimal difference;

}
