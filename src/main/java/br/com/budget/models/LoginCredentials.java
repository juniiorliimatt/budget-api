package br.com.budget.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LoginCredentials {

  private String email;
  private String password;

}
