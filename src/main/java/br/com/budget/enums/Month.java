package br.com.budget.enums;

public enum Month {

  JANUARY("Janeiro"),
  FEBRUARY("Fevereiro"),
  MARCH("Março"),
  APRIL("Abril"),
  MAY("Maio"),
  JUNE("Junho"),
  JULY("Julho"),
  AUGUST("Agosto"),
  SEPTEMBER("Setembro"),
  OCTOBER("Outubro"),
  NOVEMBER("Novembro"),
  DECEMBER("Dezembro");

  private final String value;

  Month(String value) {
	this.value = value;
  }

  public String getValue() {
	return value;
  }

}
