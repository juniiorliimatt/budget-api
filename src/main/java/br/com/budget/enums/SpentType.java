package br.com.budget.enums;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 13/05/2022
 */
public enum SpentType {

  ESSENTIAL("Essencial"),
  PERSONAL("Pessoal"),
  SAVINGS("Economia");

  private final String value;

  SpentType(String value) {
	this.value = value;
  }

  public String getValue() {
	return value;
  }
}
