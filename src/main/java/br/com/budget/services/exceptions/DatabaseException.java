package br.com.budget.services.exceptions;

public class DatabaseException extends RuntimeException {

  public DatabaseException(String message) {
	super(message);
  }

}
