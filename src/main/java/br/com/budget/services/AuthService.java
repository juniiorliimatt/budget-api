package br.com.budget.services;

import br.com.budget.entities.Role;
import br.com.budget.entities.User;
import br.com.budget.models.LoginCredentials;
import br.com.budget.models.dto.UserDTO;
import br.com.budget.repositories.RoleRepository;
import br.com.budget.repositories.UserRepository;
import br.com.budget.security.JWTUtil;
import br.com.budget.services.exceptions.LoginInvalidException;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final JWTUtil jwtUtil;
    private final AuthenticationManager authenticationManager;
    private final BCryptPasswordEncoder passwordEncoder;

    public Map<String, String> register(final UserDTO userDTO) {
        final var user = new User();
        BeanUtils.copyProperties(userDTO, user);
        if(!userDTO.getRoles().isEmpty()) {
            final var roles = userDTO.getRoles().stream().map((role) ->
                    roleRepository.findById(role.getId()).orElseThrow(() -> new ResourceNotFoundException("Role not found"))).collect(Collectors.toSet());
            user.setRoles(roles);
        }
        String encodedPass = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPass);
        final var newUser = userRepository.save(user);
        BeanUtils.copyProperties(newUser, userDTO);
        String token = jwtUtil.generateToken(userDTO.getEmail());
        return Collections.singletonMap("jwt_token", token);
    }

    public Map<String, String> login(final LoginCredentials loginCredentials) {
        try {
            final var authInputToken = new UsernamePasswordAuthenticationToken(loginCredentials.getEmail(), loginCredentials.getPassword());
            authenticationManager.authenticate(authInputToken);
            final var user = userRepository.findByEmail(loginCredentials.getEmail());
            String token = jwtUtil.generateToken(loginCredentials.getEmail());
            final var map = new HashMap<String, String>();
            map.put("jwt_token", token);
            map.put("user", user.isPresent() ? user.get().getFirstName() : "No user");
            return map;
        } catch (AuthenticationException exception) {
            throw new LoginInvalidException("Invalid Login Credentials");
        }
    }
}
