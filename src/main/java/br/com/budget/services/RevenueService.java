package br.com.budget.services;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.entities.Revenue;
import br.com.budget.models.FiftyThirtyTwenty;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.models.dto.RevenueDTO;
import br.com.budget.models.dto.TotalDTO;
import br.com.budget.repositories.RevenueRepository;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RevenueService {

    private static final String REVENUE_NOT_FOUND = "Revenue not found";
    private final RevenueRepository revenueRepository;

    @Transactional(readOnly = true)
    public Page<RevenueDTO> findAll(final Pageable pageable) {
        return revenueRepository.findAll(pageable).map(RevenueDTO::new);
    }

    @Transactional(readOnly = true)
    public RevenueDTO findById(final EntityIdDTO entityIdDTO) {
        final var revenue = revenueRepository.findById(entityIdDTO.getId())
                                       .orElseThrow(() -> new ResourceNotFoundException(REVENUE_NOT_FOUND));
        return new RevenueDTO(revenue);
    }

    @Transactional(readOnly = true)
    public Page<RevenueDTO> findByDate(final Pageable pageable, final LocalDate date) {
        return revenueRepository.findByDate(pageable, date).map(RevenueDTO::new);
    }

    @Transactional(readOnly = true)
    public Optional<Revenue> findByDate(final LocalDate date) {
        return revenueRepository.findByDate(date);
    }

    @Transactional
    public RevenueDTO save(final RevenueDTO revenueDTO) {
        final var revenue = new Revenue();
        BeanUtils.copyProperties(revenueDTO, revenue);
        revenueDTO.getAdditionalIncoming().forEach(additionalIncome -> revenue.getAdditionalIncoming().add(additionalIncome));
        final var newRevenue = revenueRepository.save(revenue);
        BeanUtils.copyProperties(newRevenue, revenueDTO);
        return revenueDTO;
    }

    @Transactional
    public RevenueDTO update(final RevenueDTO revenueDTO) {
        final var revenue = revenueRepository.findById(revenueDTO.getId())
                                       .orElseThrow(() -> new ResourceNotFoundException(REVENUE_NOT_FOUND));
        BeanUtils.copyProperties(revenueDTO, revenue);
        revenue.getAdditionalIncoming().forEach(additionalIncome -> additionalIncome.setRevenue(revenue));
        final var newRevenue = revenueRepository.save(revenue);
        BeanUtils.copyProperties(newRevenue, revenueDTO);
        return revenueDTO;
    }

    @Transactional
    public void delete(final EntityIdDTO entityIdDTO) {
        final var revenue = revenueRepository.findById(entityIdDTO.getId())
                                       .orElseThrow(() -> new ResourceNotFoundException(REVENUE_NOT_FOUND));
        revenueRepository.deleteById(revenue.getId());
    }

    @Transactional(readOnly = true)
    public FiftyThirtyTwenty fiftythirtytwenty(final LocalDate date) {
        final var month = date.getMonth().toString();
        final var year = date.getYear();

        var value = revenueRepository.findByName( month + "-" + year);

        var essentials50 = BigDecimal.ZERO;
        var personal30 = BigDecimal.ZERO;
        var savings20 = BigDecimal.ZERO;

        if(value.isPresent()){
            essentials50 = value.get().getWage().multiply(BigDecimal.valueOf(0.50));
            personal30 = value.get().getWage().multiply(BigDecimal.valueOf(0.30));
            savings20 = value.get().getWage().multiply(BigDecimal.valueOf(0.20));
        }

        return new FiftyThirtyTwenty(essentials50, personal30, savings20);
    }

    @Transactional(readOnly = true)
    public TotalDTO getTotal(final String name) {
        var revenue = revenueRepository.findByName(name).orElseThrow(() -> new ResourceNotFoundException(REVENUE_NOT_FOUND));
        var totalAdditionalIncome = revenue.getAdditionalIncoming().stream().map(AdditionalIncome::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
        var total = BigDecimal.ZERO;
        total = total.add(totalAdditionalIncome).add(revenue.getWage());
        return new TotalDTO(total);
    }
}
