package br.com.budget.services;

import br.com.budget.entities.Spending;
import br.com.budget.enums.SpentType;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.models.dto.GrandTotalDTO;
import br.com.budget.models.dto.SpendingDTO;
import br.com.budget.models.dto.TotalDTO;
import br.com.budget.models.dto.TotalExpensePerMonthDTO;
import br.com.budget.repositories.RevenueRepository;
import br.com.budget.repositories.SpendingRepository;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author Junior Lima - juniiorliimatt@gmail.com
 * @since 30/05/2022
 */


@Service
@RequiredArgsConstructor
public class SpendingService {

    private static final String SPENDING_NOT_FOUND = "Spending not found";
    private final SpendingRepository spendingRepository;

    private final RevenueRepository revenueRepository;

    private final RevenueService revenueService;

    @Transactional(readOnly = true)
    public Page<SpendingDTO> findAll(final Pageable pageable) {
        return spendingRepository.findAll(pageable).map(SpendingDTO::new);
    }

    @Transactional(readOnly = true)
    public TotalExpensePerMonthDTO getTotalExpensePerMonth(final LocalDate dateInitial, final LocalDate dateFinal) {
        final var totalExpense = spendingRepository.findByDateBetween(dateInitial, dateFinal)
                                                   .stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);

        final var paidOut = spendingRepository.findByDateBetweenAndWasPaidIsTrue(dateInitial, dateFinal)
                                              .stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);

        final var missing = spendingRepository.findByDateBetweenAndWasPaidIsFalse(dateInitial, dateFinal)
                                              .stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);

        final var total = revenueService.getTotal(dateInitial.getMonth() + "-" + LocalDate.now().getYear()).getTotal();

        var finalBalance = BigDecimal.ZERO;
        var currentBalance = BigDecimal.ZERO;

        finalBalance = finalBalance.add(total).subtract(totalExpense);
        currentBalance = currentBalance.add(total).subtract(paidOut);

        return TotalExpensePerMonthDTO.builder()
                                      .totalSpend(totalExpense)
                                      .paidOut(paidOut)
                                      .missing(missing)
                                      .finalBalance(finalBalance)
                                      .currentBalance(currentBalance)
                                      .build();
    }

    @Transactional(readOnly = true)
    public SpendingDTO findById(final EntityIdDTO entityIdDTO) {
        final var spending = spendingRepository.findById(entityIdDTO.getId()).orElseThrow(() -> new ResourceNotFoundException(SPENDING_NOT_FOUND));
        return new SpendingDTO(spending);
    }

    @Transactional(readOnly = true)
    public Page<SpendingDTO> findByDate(final Pageable pageable, final LocalDate date) {
        return spendingRepository.findByDate(pageable, date).map(SpendingDTO::new);
    }

    @Transactional
    public SpendingDTO save(final SpendingDTO spendingDTO) {
        final var spending = new Spending();
        BeanUtils.copyProperties(spendingDTO, spending);
        final var newSpending = spendingRepository.save(spending);
        BeanUtils.copyProperties(newSpending, spendingDTO);
        return spendingDTO;
    }

    @Transactional
    public SpendingDTO update(final SpendingDTO spendingDTO) {
        final var spending = spendingRepository.findById(spendingDTO.getId()).orElseThrow(() -> new ResourceNotFoundException(SPENDING_NOT_FOUND));
        BeanUtils.copyProperties(spendingDTO, spending);
        final var newSpending = spendingRepository.save(spending);
        BeanUtils.copyProperties(newSpending, spendingDTO);
        return spendingDTO;
    }

    @Transactional
    public void delete(final EntityIdDTO entityIdDTO) {
        final var spending = spendingRepository.findById(entityIdDTO.getId()).orElseThrow(() -> new ResourceNotFoundException(SPENDING_NOT_FOUND));
        spendingRepository.deleteById(spending.getId());
    }

    @Transactional(readOnly = true)
    public TotalDTO getTotal(final LocalDate dateInitial, final LocalDate dateFinal) {
        var totalSpending = spendingRepository.findByDateBetween(dateInitial, dateFinal).stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
        return new TotalDTO(totalSpending);
    }

    @Transactional(readOnly = true)
    public GrandTotalDTO getGrandTotal(final LocalDate dateInitial, final LocalDate dateFinal, SpentType spentType){

        final var spendTotal = spendingRepository.findByDateBetweenAndSpentType(dateInitial, dateFinal, spentType).stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
        final var paidTotal = spendingRepository.findByDateBetweenAndWasPaidIsTrueAndSpentType(dateInitial, dateFinal, spentType).stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);
        final var missingTotal = spendingRepository.findByDateBetweenAndWasPaidIsFalseAndSpentType(dateInitial, dateFinal, spentType).stream().map(Spending::getValue).reduce(BigDecimal.ZERO, BigDecimal::add);

        var difference = BigDecimal.ZERO;

        if(spentType.getValue().equals("Essencial")){
            final var total = revenueService.fiftythirtytwenty(dateInitial).getEssentials50();
            difference = paidTotal.subtract(total);
        }

        if(spentType.getValue().equals("Pessoal")){
            final var total = revenueService.fiftythirtytwenty(dateInitial).getPersonal30();
            difference = paidTotal.subtract(total);
        }

        if(spentType.getValue().equals("Economia")){
            final var total = revenueService.fiftythirtytwenty(dateInitial).getSavings20();
            difference = paidTotal.subtract(total);
        }

        return GrandTotalDTO.builder().spendTotal(spendTotal).paidTotal(paidTotal).missingTotal(missingTotal).difference(difference).build();
    }
}
