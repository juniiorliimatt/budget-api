package br.com.budget.services;

import br.com.budget.entities.AdditionalIncome;
import br.com.budget.models.dto.AdditionalIncomeDTO;
import br.com.budget.models.dto.EntityIdDTO;
import br.com.budget.repositories.AdditionalIncomeRepository;
import br.com.budget.services.exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AdditionalIncomeService {

    private static final String ADDITIONAL_INCOME_NOT_FOUND = "Additional Income not found";
    private final AdditionalIncomeRepository repository;

    @Transactional(readOnly = true)
    public Page<AdditionalIncomeDTO> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(AdditionalIncomeDTO::new);
    }

    @Transactional(readOnly = true)
    public List<AdditionalIncomeDTO> findByName(String name) {
        var additionalIncomeList = repository.findByNameContainingIgnoreCase(name);
        return additionalIncomeList.stream().map(AdditionalIncomeDTO::new).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public AdditionalIncomeDTO findById(EntityIdDTO entityIdDTO) {
        var additionalIncome = repository.findById(entityIdDTO.getId())
                                         .orElseThrow(
                                                 () -> new ResourceNotFoundException(ADDITIONAL_INCOME_NOT_FOUND));
        return new AdditionalIncomeDTO(additionalIncome);
    }

    @Transactional
    public AdditionalIncomeDTO save(AdditionalIncomeDTO additionalIncomeDTO) {
        var additionalIncome = new AdditionalIncome();
        BeanUtils.copyProperties(additionalIncomeDTO, additionalIncome);
        additionalIncome = repository.save(additionalIncome);
        return new AdditionalIncomeDTO(additionalIncome);
    }

    @Transactional
    public AdditionalIncomeDTO update(AdditionalIncomeDTO additionalIncomeDTO) {
        var additionalIncome = repository.findById(additionalIncomeDTO.getId()).orElseThrow(
                () -> new ResourceNotFoundException(ADDITIONAL_INCOME_NOT_FOUND));
        BeanUtils.copyProperties(additionalIncomeDTO, additionalIncome);
        additionalIncome = repository.save(additionalIncome);
        return new AdditionalIncomeDTO(additionalIncome);
    }

    @Transactional
    public void deleteById(EntityIdDTO entityIdDTO) {
        var additionalIncome = repository.findById(entityIdDTO.getId()).orElseThrow(
                () -> new ResourceNotFoundException(ADDITIONAL_INCOME_NOT_FOUND));
        repository.deleteById(additionalIncome.getId());
    }

}
